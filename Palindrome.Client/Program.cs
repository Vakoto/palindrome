﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Palindrome.Client.Utils;

namespace Palindrome.Client
{
    class Program
    {
        private static async Task<string> IsPalindromeWithRetries(
            HttpClient client, 
            Uri uri, 
            string content,
            int retriesCount = 10)
        {
            var delay = TimeSpan.FromSeconds(1);
            var stringContent = new StringContent(content, Encoding.UTF8);
            for (var i = 0; i < retriesCount; i++)
            {
                var response = await client.PostAsync(uri, stringContent);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return await response.Content.ReadAsStringAsync();
                }

                await Task.Delay(delay);
            }

            throw new OperationCanceledException("Too many retries.");
        }

        private static bool Ping(Uri uri)
        {
            try
            {
                var request = (HttpWebRequest)HttpWebRequest.Create(uri);
                request.Timeout = 3000;
                request.Method = "GET";

                using (var response = request.GetResponse())
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static async Task Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("First argument must contain the folder path");
                return;
            }

            var directoryPath = args[0];
            if (!Directory.Exists(directoryPath))
            {
                Console.WriteLine($"Path {directoryPath} doesn't exist");
                return;
            }

            var maxConcurrencyRequests = 30;
            if (args.Length > 1)
            {
                maxConcurrencyRequests = int.Parse(args[1]);
            }

            var directory = new DirectoryInfo(directoryPath);
            var files = directory.GetFiles();
            var uri = new Uri("http://127.0.0.1:8000/palindrome/");

            if (!Ping(uri))
            {
                Console.WriteLine("Remote host is not responding.");
                return;
            }

            using (var client = new HttpClient())
            {
                var tasks = files.Select(async file =>
                {
                    try
                    {
                        var text = await File.ReadAllTextAsync(file.FullName, Encoding.UTF8);
                        var result = await IsPalindromeWithRetries(client, uri, text);
                        Console.WriteLine($"File = {file.Name}, Result = {result}");
                    }
                    catch (HttpRequestException e)
                    {
                        Console.WriteLine($"Exception Caught! Message: {e.Message}");
                    }
                    catch (OperationCanceledException e)
                    {
                        Console.WriteLine($"Operation ended with an error for {file.Name}: {e.Message}");
                    }
                });

                foreach (var part in tasks.Partition(maxConcurrencyRequests))
                {
                    await Task.WhenAll(part);
                }
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
