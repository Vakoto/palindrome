﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Palindrome.Server
{
    public class Server
    {
        public string Uri { get; }

        public int MaxConcurrentRequests { get; }

        private readonly Func<string, string> handler;

        public Server(
            string uri, 
            int maxConcurrentRequests,
            Func<string, string> handler)
        {
            this.handler = handler;
            Uri = uri;
            MaxConcurrentRequests = maxConcurrentRequests;
        }

        public async Task ListenAsync()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add(Uri);
            listener.Start();
            Log($"Listening for connections on {Uri} with MaxConcurrentRequests = {MaxConcurrentRequests}");

            var semaphoreSlim = new SemaphoreSlim(MaxConcurrentRequests + 1, MaxConcurrentRequests + 1);

            while (true)
            {
                var context = await listener.GetContextAsync();

                _ = Task.Run(() => HandleClientAsync(context, semaphoreSlim))
                    .ContinueWith(t =>
                            Log(t.Exception?.Message),
                        TaskContinuationOptions.OnlyOnFaulted
                    ).ConfigureAwait(false);
            }
        }

        private async Task HandleClientAsync(HttpListenerContext ctx, SemaphoreSlim semaphoreSlim)
        {
            var success = await semaphoreSlim.WaitAsync(TimeSpan.Zero);
            if (!success)
            {
                ReportFailure(ctx.Response);
                Log(ctx.Request.RequestTraceIdentifier,
                    $"requestCount max size reached: {MaxConcurrentRequests}. Connection refused with 503 error.");
                return;
            }

            try
            {
                await HandleClientAsync(ctx);
            }
            finally
            {
                semaphoreSlim.Release();
            }
        }

        private void ReportFailure(HttpListenerResponse response)
        {
            response.StatusCode = 503;
            response.OutputStream.Close();
            response.Close();
        }

        private async Task HandleClientAsync(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;

            Log(request.RequestTraceIdentifier, $"{request.Url} {request.HttpMethod} {request.UserHostName} {request.UserAgent}");
            var str = await ReadBodyAsync(request);
            var result = handler(str);
            await WriteResultAsync(response, result);
            response.Close();

            await Task.Delay(TimeSpan.FromSeconds(2));
        }

        private void Log(Guid requestTraceIdentifier, string message)
        {
            Log($"{requestTraceIdentifier} -> {message}");
        }

        private void Log(string message)
        {
            Console.WriteLine($"{DateTime.Now}: {message}");
        }

        private async Task<string> ReadBodyAsync(HttpListenerRequest request)
        {
            if (!request.HasEntityBody)
            {
                Log(request.RequestTraceIdentifier, "No client data was sent with the request.");
                return string.Empty;
            }

            await using (var body = request.InputStream)
            {
                var encoding = request.ContentEncoding;
                using (var reader = new StreamReader(body, encoding))
                {
                    if (request.ContentType != null)
                    {
                        Log(request.RequestTraceIdentifier, $"Client data content type {request.ContentType}");
                    }

                    Log(request.RequestTraceIdentifier, $"Client data content length {request.ContentLength64}");

                    return await reader.ReadToEndAsync();
                }
            }
        }

        private async Task WriteResultAsync(HttpListenerResponse response, string result)
        {
            var data = Encoding.UTF8.GetBytes(result);

            response.ContentType = "text/plain";
            response.ContentEncoding = Encoding.UTF8;
            response.ContentLength64 = data.LongLength;

            await response.OutputStream.WriteAsync(data, 0, data.Length);
        }
    }
}
