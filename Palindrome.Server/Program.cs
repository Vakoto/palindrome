﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Palindrome.Server
{
    class Program
    {
        private static bool IsPalindrome(string str)
        {
            var s = str
                .Where(char.IsLetterOrDigit)
                .Select(char.ToLower)
                .ToArray();

            for (var i = 0; i < s.Length / 2; i++)
            {
                if (s[i] != s[s.Length - i - 1])
                {
                    return false;
                }
            }

            return true;
        }

        public static async Task Main(string[] args)
        {
            int maxConcurrentRequests = 5;
            if (args.Length > 0)
            {
                if (int.TryParse(args[0], out var n))
                {
                    maxConcurrentRequests = n;
                }
            }

            var server = new Server(
                "http://127.0.0.1:8000/palindrome/",
                maxConcurrentRequests,
                str => IsPalindrome(str).ToString()
            );

            await server.ListenAsync();
        }
    }
}
